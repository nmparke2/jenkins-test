<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Register New User</title>
</head>
<body>
<h1>Register</h1>
<form action="/add-user" method="post" modelAttribute="userForm">
	<table>
		<tr>
			<td>Email</td>
		</tr>
		<tr>
			<td><input type="text" name="email"/></td>
		</tr>
		<tr>
			<td>Username</td>
		</tr>
		<tr>
			<td><input type="text" name="userName"/></td>
		</tr>
		<tr>
			<td>Password</td>
		</tr>
		<tr>
			<td><input type="password" name="password"/></td>
		</tr>
		<tr>
			<td>First Name</td>
		</tr>
		<tr>
			<td><input type="text" name="firstName"/></td>
		</tr>
		<tr>
			<td>Last Name</td>
		</tr>
		<tr>
			<td><input type="text" name="lastName"/></td>
		</tr>
		<tr>
			<td>Phone Number</td>
		</tr>
		<tr>
			<td><input type="text" name="phoneNumber"/></td>
		</tr>
		<tr>
			<td>Address</td>
		</tr>
		<tr>
			<td><input type="text" name="address"/></td>
		</tr>
	</table>
</form>

</body>
</html>