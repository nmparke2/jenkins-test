package com.yash.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.yash.model.User;
import com.yash.model.UserDetail;
import com.yash.service.UserDetailService;
import com.yash.service.UserService;

@Controller
public class UserController {
	
	@Autowired
	private UserService userService;
	@Autowired
	private UserDetailService userDetailService;
	
	@GetMapping("/login")
	//public String login(Model model, String error) {
		if (error != null)
			model.addAttribute("error", "Your username and password is invalid.");

		return "login";
	}
	
	@GetMapping("/register")
	public String register(Model model, String error) {
		if (error != null)
			model.addAttribute("error", "Your username and password is invalid.");

		return "register";
	}
	
	@PostMapping("/add-user")
	public String addUser(Model model, @ModelAttribute("userForm") User user, @ModelAttribute("userDetailForm") UserDetail userDetail) {
		user.setUserDetail(userDetail);
		userDetail.setUser(user);
		userService.save(user);
		return "login-success";
	}
}
