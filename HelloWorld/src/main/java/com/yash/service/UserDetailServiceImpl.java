package com.yash.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.yash.model.UserDetail;
import com.yash.repository.UserDetailRepository;

@Service
public class UserDetailServiceImpl implements UserDetailService{

	@Autowired
	private UserDetailRepository userDetailRepository;
	
	@Override
	public void save(UserDetail userDetail) {
		userDetailRepository.save(userDetail);
	}

}
